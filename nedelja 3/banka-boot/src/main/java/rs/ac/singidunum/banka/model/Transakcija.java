package rs.ac.singidunum.banka.model;

public class Transakcija {
	private String idTransakcije;
	private Racun posiljalac;
	private Racun primalac;
	private double iznos;
	
	public Transakcija() {
		
	}

	public Transakcija(String idTransakcije, Racun posiljalac, Racun primalac, double iznos) {
		super();
		this.idTransakcije = idTransakcije;
		this.posiljalac = posiljalac;
		this.primalac = primalac;
		this.iznos = iznos;
	}

	public String getIdTransakcije() {
		return idTransakcije;
	}

	public void setIdTransakcije(String idTransakcije) {
		this.idTransakcije = idTransakcije;
	}

	public Racun getPosiljalac() {
		return posiljalac;
	}

	public void setPosiljalac(Racun posiljalac) {
		this.posiljalac = posiljalac;
	}

	public Racun getPrimalac() {
		return primalac;
	}

	public void setPrimalac(Racun primalac) {
		this.primalac = primalac;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}
}
