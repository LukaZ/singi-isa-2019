package rs.ac.singidunum.banka.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ac.singidunum.banka.model.Racun;
import rs.ac.singidunum.banka.services.RacunService;

@RestController
@RequestMapping("/api/racuni")
public class RacunController {
	@Autowired
	RacunService rs;
	
	@RequestMapping()
	public ResponseEntity<List<Racun>> dobavljanjeRacuna() {
		return new ResponseEntity<List<Racun>>(rs.getRacuni(), HttpStatus.OK);
	}
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<Racun> dodavanjeRacuna(@RequestBody Racun racun) {
		rs.addRacun(racun);
		return new ResponseEntity<Racun>(racun, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Racun> dobavljaneJednogRacuna(@PathVariable String id) {
		Racun racun = rs.getRacun(id);
		if(racun != null) {
			return new ResponseEntity<Racun>(racun, HttpStatus.OK);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Racun> uklanjanjeJednogRacuna(@PathVariable String id) {
		Racun racun = rs.getRacun(id);
		if(racun != null) {
			rs.removeRacun(racun);
			return new ResponseEntity<Racun>(HttpStatus.OK);
		}
		return new ResponseEntity<Racun>(HttpStatus.NOT_FOUND);
	}
}
