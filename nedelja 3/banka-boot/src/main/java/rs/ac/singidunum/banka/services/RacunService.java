package rs.ac.singidunum.banka.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import rs.ac.singidunum.banka.model.Racun;

@Service
public class RacunService {
	private ArrayList<Racun> racuni;
	
	public RacunService() {
		racuni = new ArrayList<Racun>();
		racuni.add(new Racun("551231413", 12500.0));
		racuni.add(new Racun("551255112", 10000.0));
		racuni.add(new Racun("661251245", 10600.0));
		racuni.add(new Racun("882346732", 20000.0));
		racuni.add(new Racun("991238142", 17000.0));
	}
	
	public ArrayList<Racun> getRacuni() {
		return racuni;
	}
	
	public Racun getRacun(String id) {
		for(Racun r : racuni) {
			if(r.getIdRacuna().equals(id)) {
				return r;
			}
		}
		
		return null;
	}
	
	public void addRacun(Racun racun) {
		racuni.add(racun);
	}
	
	public void removeRacun(Racun racun) {
		int i = 0;
		for(; i < racuni.size(); i++) {
			if(racuni.get(i).getIdRacuna().equals(racun.getIdRacuna())) {
				racuni.remove(i);
				return;
			}
		}
		
	}
}
