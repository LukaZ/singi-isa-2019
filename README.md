# singi-isa-2019

Raspored tema po nedeljama nastave:

## 1) 26.2.2019.utorak
    - HTTP protokol podsecanje
    - web server/app server/ - podesecanje
    - zašto nam treba Spring okruženje
    - kako pripremiti jednostavan Java projekat u Eclipse IDE 
    - zašto nam treba maven i kako se podešava
    - jednostavan Spring projekat sa jednom komponentom 
    - spakovati jar i pokrenuti ga

## 2) 5.3.2019. utorak
     - komponente i povezivanje
     - spring boot 
     - DI - dependancy injection, podsecanje na interface, nasledjivanje...
     - AOP - aspekti na jednostavnim primerima
     - Kreirati jednostavnu java app gde komponente komuniciraju
     - Primer klijent server komunikacija Socket

## 3) 12.3.2019. utorak
      - slojevita arhitektura
      - MVC 
      - repozitorijum bez baze
      - napisati testove za kontrolu 
      - napraviti aplikaciju u kojoj su komponente rasporedjene u slojeve
      - napraviti jar sa automatskim pokretanjem testova i deployment na tommy server 

## 4) 19.3.2019. utorak
    - ORM, Hibernate, JPA
    - repozitorijum MySQL
    - primer iz trece nedelje dopuniti entitetima koji se nalaze u bazi a veze su:
          1-1
          1-N
          N-N
    - dopuniti testove
    - napraviti jar i u raditi deployment na tommy server

## 5) 26.3.2019. utorak
    - Event driven arhitektura
    - Producer - consumer, Message service, 
    - modeliranje formata poruka/ binarne, nestruktuirane, struktuirane...
    - napraviti nekoliko producera i nekoliko consumera 
    - naivno povezivanje (produceri i consumeri znaju IP adrese)
## 6) 2.4.2019. utorak
    - Kolokvijum
    - ocena 6: napisati jednostavnu slojevitu back end applikaciju sa jednim entitetom i koriscenjem ORM
    - ocena 8: prosiriti primer za ocenu 6 komplikovanijim poveivanjem komponenti
    - ocena 10: dopuniti primer za ocenu 8 sa event driven demonstracijom
## 7) 9.4.2019. utorak
    - Mikro kernel arhitektura
    - Znacaj plugin-a 
    - Automatsko povezivanje i skaliranje 
    - Primer: kreirati repozitorijum dokumenata gde se dokumenti smestaju u BLOB polju u bazi 
## 8) 16.4.2019. utorak
    - Mikro servisna arhitektura
    - Registracija servisa, security, load balancing
    - Primer: Kreiranje servisa za rucno kreiranje rasporeda. Entiteti: studenti, grupe, nastavnici, predmeti, ucionice 
    - napraviti jar, pokrenuti testove i uraditi deployment na tommy server
## 9) 23.4.2019. utorak
    - Prostorno bazirane arhitekture
    - Primer: Sistem u kojem nekoliko jednostavnih sistema za rezervaciju parkinga saradjuju 
    - napraviti jar, pokrenuti test, uraditi deployment, pokrenuti simulaciju

## 10) 30.4.2019. utorak
    - Agent orijentisana arhitektura
    - Umesto toka podataka uraditi tok agenata koji obradjuju podatke
    - Pojam transakcija, izolacije, oporavak
## 11) 7.5.2019. utorak
    - priprema za kolokvijum


## 12) 14.5.2019. utorak
    - Kolokvijum II
    - ocena 6: napisati jednostavnu aplikaciju baziranu na mikroservisnoj arhitekturi
    - ocena 8: prosiriti primer sa security aspektima
    - ocena 10: prisiriti primer sa transakcijama i testovima za proveru
    Primeri: uplata i prijava ispita

## 13) 21.5.2019. utorak
    - IoT primeri i implementacije u C i C++
    - Primer: prisustvo na predavnjima 
## 14) 28.5.2019. utorak
    Zavrsni projekti/ priprema
## 15) 4.6.2019. utorak
    Zavrsni projekti/ priprema
