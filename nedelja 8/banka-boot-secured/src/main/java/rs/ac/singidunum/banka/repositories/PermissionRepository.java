package rs.ac.singidunum.banka.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.banka.model.Permission;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Long>{

}
