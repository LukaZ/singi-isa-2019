package rs.ac.singidunum.banka.repositories;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.banka.model.UserPermission;

@Repository
public interface UserPermissionRepository extends CrudRepository<UserPermission, Long>{
	Set<UserPermission> getByUserId(Long id);
}
