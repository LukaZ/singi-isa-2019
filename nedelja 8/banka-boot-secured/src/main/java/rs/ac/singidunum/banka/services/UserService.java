package rs.ac.singidunum.banka.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.banka.model.User;
import rs.ac.singidunum.banka.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public Optional<User> getUser(String username) {
		return userRepository.getByUsername(username);
	}
	
	public Optional<User> getUser(String username, String password) {
		return userRepository.getByUsernameAndPassword(username, password);
	}
}
