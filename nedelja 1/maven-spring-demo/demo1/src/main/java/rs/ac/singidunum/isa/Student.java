package rs.ac.singidunum.isa;

public class Student {

	String ime;
	String prezime;
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	
	public void test() {
		System.out.println("Proba");
	}
	
	
	@Override
	public String toString() {
		return "Student [ime=" + ime + ", prezime=" + prezime + "]";
	}

	
}
