package rs.ac.singidunum.isa;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {
	public String sample() {
		return "Nesto";
	}
	
	public static void main(String args[]) {
	      ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
	      Student obj1 = (Student) context.getBean("student");
	      obj1.test();
	      
	      System.out.println(obj1);
	}

}
