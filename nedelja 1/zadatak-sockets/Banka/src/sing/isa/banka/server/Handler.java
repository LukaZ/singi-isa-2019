package sing.isa.banka.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Handler implements Runnable {
	private Socket socket;
	private Racun racun;

	public Handler(Socket socket, Racun racun) {
		this.socket = socket;
		this.racun = racun;
	}

	@Override
	public void run() {
		try {
			PrintWriter os = new PrintWriter(socket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			System.out.println("Konekcija pristigla od: " + socket.getInetAddress().toString());
			String input = in.readLine();

			while (!input.equals("STOP")) {
				System.out.println("Izmena stanja: " + input);
				double iznos = Double.parseDouble(input);

				if (iznos <= 0 && racun.getRaspolozivo() > 0) {
					racun.setRaspolozivo(racun.getRaspolozivo() + iznos);
					os.println("Sredstva oduzeta");
					os.flush();
				} else if (iznos >= 0 && racun.getRaspolozivo() < 100000) {
					racun.setRaspolozivo(racun.getRaspolozivo() + iznos);
					os.println("Sredstva dodata");
					os.flush();
				} else {
					os.println("Neuspela transakcija!");
					os.flush();
				}
				
				input = in.readLine();
			}

			os.close();
			in.close();
			socket.close();

			System.out.println("Stanje: " + racun.getRaspolozivo());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
