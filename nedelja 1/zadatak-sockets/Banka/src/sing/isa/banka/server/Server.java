package sing.isa.banka.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) {
		Racun racun = new Racun(10000);

		int port = 3000;
		try {
			ServerSocket ss = new ServerSocket(port);
			System.out.println("Server slu�a na port: " + port);

			while (true) {
				Socket clientSocket = ss.accept();
				Handler h = new Handler(clientSocket, racun);
				Thread t = new Thread(h);
				t.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Neuspe�no pokretanje servera.");
		}
	}

}
