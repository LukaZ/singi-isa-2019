package rs.singidunum.api1.first;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {

	private List<Topic> lista;
	
	public TopicService(){
		lista = new ArrayList<Topic>();
		lista.add(new Topic("1", "proba"));
		lista.add(new Topic("2", "dva"));		
	}
	
	public List<Topic> all(){
		return lista;
	}
	
	public void add(Topic topic) {
		lista.add(topic);
	}

	public Topic get(String id) {
		for(Topic el: lista) {
			if(el.getId().equals(id))
				return el;
		}
		return null;
	}
	
}
