package sing.isa.banka.client;

public class Transfer {
	public String r1;
	public String r2;
	public double iznos;
	
	public Transfer(String r1, String r2, double iznos) {
		this.r1 = r1;
		this.r2 = r2;
		this.iznos = iznos;
	}
	
	public String toString() {
		return "transfer;" + r1 + ";" + r2 + ";" + iznos;
	}
}
