package jpa02;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Student {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String name;
	private String indeks;
	
	@OneToMany(mappedBy="vlasnik")
	private List<Racun> racuni;
	
	
	public Student() {}
	
	
	public Student(String name) {
		super();
		this.name = name;
	}
	
	public String getIndeks() {
		return this.indeks;
	}
	
	public void setIndeks(String indeks) {
		this.indeks = indeks;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	public List<Racun> getRacuni() {
		return racuni;
	}


	public void setRacuni(List<Racun> racuni) {
		this.racuni = racuni;
	}
	
	
	
	
}
