package jpa02;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/students")
public class StudentsController {

	@Autowired
	StudentRepository students;
	
	@Autowired
	RacuniRepository racuni;
	
	
	@PutMapping("/")
	public Student create(@RequestBody Student student) {
		return students.save(student);
	}
	
	@GetMapping("/")
	public List<Student> getAll(){
		return (List<Student>)students.findAll();
	}
	
	
	@GetMapping("/{id}")
	public Optional<Student> getById(@PathVariable("id") long id)
	{
		Optional<Student> rr = students.findById(id);
		return rr;
	}

	@PostMapping("/{id}")
	public Student update(@PathVariable("id") long id, @RequestBody Student student) {
		Optional<Student> st = students.findById(id);
		if(st.isPresent()) {
			return students.save(student);
		}
		return null;
	}
	
	@DeleteMapping("/{id}")
	public String delete(@PathVariable("id") long id) {
		Optional<Student> student = students.findById(id);
		if(student.isPresent()) {
			students.deleteById(id);
		}
		return "{\"rez\":\"ok\"}";
	}
	
	@PutMapping("/{id}/racuni")
	public Racun createRacun(@PathVariable("id") long id, @RequestBody Racun racun) {
		
		Optional<Student> st = students.findById(id);
		if(st.isPresent()) {
			racun.setVlasnik(st.get());
			return racuni.save(racun);
		}
		return null;
	}
	
	@GetMapping("/{id}/racuni")
	public List<Racun> studentoviRacuni(@PathVariable("id") long id){
		Optional<Student> student = students.findById(id);
		if(student.isPresent()) {
			return racuni.findByVlasnik(student.get());
		}
		return null;
	}
	
	
}
 