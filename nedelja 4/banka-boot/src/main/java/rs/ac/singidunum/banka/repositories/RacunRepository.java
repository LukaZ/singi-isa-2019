package rs.ac.singidunum.banka.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.banka.model.Racun;

@Repository
public interface RacunRepository extends CrudRepository<Racun, Long> {
	Optional<Racun> findByBrojRacuna(String brojRacuna);
}
