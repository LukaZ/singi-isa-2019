package rs.ac.singidunum.banka.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.banka.model.Racun;
import rs.ac.singidunum.banka.repositories.RacunRepository;

@Service
public class RacunService {
	@Autowired
	private RacunRepository rr;
	
	public RacunService() {
	}
	
	public Iterable<Racun> getRacuni() {
		return rr.findAll();
	}
	
	public Optional<Racun> getRacun(Long id) {
		return rr.findById(id);
	}
	
	public void addRacun(Racun racun) {
		rr.save(racun);
	}
	
	public void removeRacun(Long id) {
		Optional<Racun> racun = rr.findById(id);
		rr.delete(racun.get());
	}
}
