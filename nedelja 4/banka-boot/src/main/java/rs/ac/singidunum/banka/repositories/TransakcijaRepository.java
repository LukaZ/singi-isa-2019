package rs.ac.singidunum.banka.repositories;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.banka.model.Transakcija;

public interface TransakcijaRepository extends CrudRepository<Transakcija, Long> {

}
