package rs.ac.singidunum.banka.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import rs.ac.singidunum.banka.model.Transakcija;
import rs.ac.singidunum.banka.services.TransakcijaService;
import rs.ac.singidunum.banka.utils.View.HideOptionalProperties;

@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
@RequestMapping("/api/transakcije")
public class TransakcijaController {
	@Autowired
	TransakcijaService ts;
	
	@JsonView(HideOptionalProperties.class)
	@RequestMapping()
	public ResponseEntity<Iterable<Transakcija>> dobavljanjeTransakcija() {
		return new ResponseEntity<Iterable<Transakcija>>(ts.getTransakcije(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Transakcija> dodavanjeTransakcije(@RequestBody Transakcija transakcija) {
		Transakcija t = ts.addTransakcija(transakcija.getPosiljalac().getBrojRacuna(),
										  transakcija.getPrimalac().getBrojRacuna(),
										  transakcija.getIznos());
		return new ResponseEntity<Transakcija>(t, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Transakcija> dobavljaneJedneTransakcije(@PathVariable Long id) {
		Optional<Transakcija> t = ts.getTransakcija(id);
		
		if(t.isPresent()) {
			return new ResponseEntity<Transakcija>(t.get(), HttpStatus.OK);
		}
		
		return new ResponseEntity<Transakcija>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Transakcija> uklanjanjeJednogRacuna(@PathVariable Long id) {
		try {
			ts.removeTransakcija(id);
		} catch (Exception e) {
			return new ResponseEntity<Transakcija>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Transakcija>(HttpStatus.NO_CONTENT);
	}
}
