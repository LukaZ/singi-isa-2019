package rs.ac.singidunum.addressplugin;

import java.util.HashMap;
import java.util.HashSet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import rs.ac.singidunum.addressplugin.plugin.PluginDescription;

@SpringBootApplication
public class App {
	public static void main(String[] args) {
		RestTemplate rt = new RestTemplate();
		PluginDescription pd = new PluginDescription("Address plugin", "address", "Address service plugin", "http://localhost:8081", new HashMap<HttpMethod, HashSet<String>>());
		
		pd.getEndpoints().put(HttpMethod.GET, new HashSet<String>());
		rt.postForLocation("http://localhost:8080/plugins", pd);
		SpringApplication.run(App.class, args);
	}
}
